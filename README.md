# Royal Crous

## Contexte Mini projet Agile

Nous avions 3 jours (Environ 24 heures au total) pour coder un jeu en mode textuel à partir de 0 ! 
Dans un groupe aléatoire (Avec des gens qu'on ne connait pas forcément) nous devions trouver un thème et des idées puis les réaliser en Java. 

Journée : 8 heures par jours -> Sprint de 2 heures et réunion régulière

### Membre

    Hocine Aliouat
    Renan Declercq
    Theo Franos
    Mounir Khatri
    Marine Sandras

### Description Royal Crous

Bienvenue au Royal Crous, Le casino des boursiers.
Venez jouer au BlackJack et à la roulette pour peut-être doubler votre bourse. 
